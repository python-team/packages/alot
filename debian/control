Source: alot
Section: mail
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Jordan Justen <jljusten@debian.org>,
 Simon Chopin <chopin.simon@gmail.com>,
 Johannes 'josch' Schauer <josch@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 procps,
 less,
 locales-all,
 pybuild-plugin-pyproject,
 python3,
 python3-configobj,
 python3-doc,
 python3-gpg (>= 1.13.1-6),
 python3-magic,
 python3-notmuch2 (>= 0.30),
 python3-setuptools,
 python3-sphinx,
 python3-standard-mailcap,
 python3-twisted,
 python3-urwid,
 python3-urwidtrees (>= 1.0.3),
Standards-Version: 4.6.2
Homepage: https://github.com/pazz/alot/
Vcs-Git: https://salsa.debian.org/python-team/packages/alot.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/alot

Package: alot
Architecture: all
Depends:
 python3-configobj,
 python3-gpg,
 python3-magic,
 python3-notmuch2 (>= 0.30),
 python3-standard-mailcap,
 python3-twisted,
 python3-urwid,
 python3-urwidtrees (>= 1.0.3),
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 alot-doc,
Recommends:
 notmuch,
 w3m | links | links2,
Description: Text mode MUA using notmuch mail
 Alot is a text mode mail user agent for the notmuch mail system.
 It features a modular and command prompt driven interface to provide
 a full MUA experience as an alternative to the Emacs and Vim modes shipped
 with notmuch.

Package: alot-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Suggests:
 python-doc,
Description: Text mode MUA using notmuch mail - documentation
 Alot is a terminal-based mail user agent for the notmuch mail system.
 It features a modular and command prompt driven interface to provide
 a full MUA experience as an alternative to the Emacs and Vim modes shipped
 with notmuch.
 .
 This package provides detailed documentation on alot usage.
