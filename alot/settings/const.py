# Copyright (C) Patrick Totzke <patricktotzke@gmail.com>
# This file is released under the GNU GPL, version 3 or a later revision.
# For further details see the COPYING file
from .manager import SettingsManager

settings = SettingsManager()
